<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Position;

class DashboardController extends Controller
{
    public function index()
    {
        $this->data['positions'] = Position::all();
        return view('index', $this->data);
    }

    public function cities($id = null)
    {
        $cities = ($id)? City::where('country_id', $id)->get() : City::all();
        return response()->json($cities);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Country;
use App\Models\Employee;
use App\Models\Position;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{

    public function __construct()
    {
        $this->data['active'] = 'employees';
    }

    public function index()
    {
        $this->data['employees'] = Employee::all();
        return view('employees.list', $this->data);
    }

    public function new()
    {
        $this->data['action'] = 'new';
        $this->data['positions'] = Position::all();
        $this->data['countries'] = Country::orderBy('name')->get();
        $this->data['cities'] = City::orderBy('name')->get();
        $bosses = Position::where('type', 1)->first();
        $this->data['bosses'] = Employee::whereNotIn('id', $bosses->employees()->pluck('id')->toArray())->get();
        return view('employees.action', $this->data);
    }

    public function edit($id)
    {
        $this->data['action'] = 'edit';
        $this->data['positions'] = Position::all();
        $this->data['employee'] = Employee::find($id);
        $this->data['countries'] = Country::orderBy('name')->get();
        $this->data['cities'] = City::orderBy('name')->get();
        $bosses = Position::where('type', 1)->first();
        $bosses = array_merge($bosses->employees()->pluck('id')->toArray(), [$id]);
        $this->data['bosses'] = Employee::whereNotIn('id', $bosses)->get();
        return view('employees.action', $this->data);
    }

    public function action(Request $request, $id = null)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $employee = ($request->get('action') == 'edit')? Employee::find($id) : new Employee;
        $employee->dni = $request->get('dni');
        $employee->name = $request->get('name');
        $employee->surname = $request->get('surname');
        $employee->address = $request->get('address');
        $employee->phone = $request->get('phone');
        $employee->country_id = $request->get('country');
        $employee->city_id = $request->get('city');
        $employee->parent_id = $request->get('boss');
        $employee->save();

        $employee->positions()->detach();
        $employee->positions()->attach($request->get('positions'));

        return redirect()->route('employees');
    }

    public function delete($id)
    {
        Employee::find($id)->delete();
        return redirect()->route('employees');
    }
}

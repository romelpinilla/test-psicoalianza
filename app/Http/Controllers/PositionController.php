<?php

namespace App\Http\Controllers;

use App\Models\Position;
use Illuminate\Http\Request;

class PositionController extends Controller
{

    public function __construct()
    {
        $this->data['active'] = 'positions';
    }

    public function index()
    {
        $this->data['positions'] = Position::all();
        return view('positions.list', $this->data);
    }

    public function new()
    {
        $this->data['action'] = 'new';
        return view('positions.action', $this->data);
    }

    public function edit($id)
    {
        $this->data['action'] = 'edit';
        $this->data['position'] = Position::find($id);
        return view('positions.action', $this->data);
    }

    public function action(Request $request, $id = null)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $position = ($request->get('action') == 'edit')? Position::find($id) : new Position;
        $position->name = $request->get('name');
        $position->save();

        return redirect()->route('positions');
    }

    public function delete($id)
    {
        Position::find($id)->delete();
        return redirect()->route('positions');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'surname',
        'address',
        'phone',
        'country_id',
        'city_id'
    ];

    public function positions() {
        return $this->belongsToMany('App\Models\Position', 'employee_position', 'employee_id', 'position_id');
    }

    public function booss() {
        return $this->belongsTo('App\Models\Employee', 'parent_id');
    }

}

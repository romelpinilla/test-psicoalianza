<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->integer('dni');
            $table->string('name', 190);
            $table->string('surname', 190);
            $table->string('address');
            $table->integer('phone');
            $table->unsignedInteger('parent_id')->nullable();
            $table->unsignedInteger('country_id');
            $table->unsignedInteger('city_id');
            $table->foreign('parent_id')->references('id')->on('employees')->onUpdate('SET NULL')->onDelete('SET NULL');
            $table->foreign('country_id')->references('id')->on('countries')->onUpdate('SET NULL')->onDelete('SET NULL');
            $table->foreign('city_id')->references('id')->on('cities')->onUpdate('SET NULL')->onDelete('SET NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}

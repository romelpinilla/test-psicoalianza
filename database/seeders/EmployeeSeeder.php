<?php

namespace Database\Seeders;

use App\Models\Employee;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Employee::create([
            'dni' => '1031115210',
            'name' => 'Romel',
            'surname' => 'Pinilla',
            'address' => 'Calle falsa 123',
            'phone' => '3140000000',
            'country_id' => 1,
            'city_id' => 1
        ]);
    }
}

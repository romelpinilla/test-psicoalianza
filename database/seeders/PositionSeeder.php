<?php

namespace Database\Seeders;

use App\Models\Position;
use Illuminate\Database\Seeder;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Position::create([
            'name' => 'Presidente de la empresa',
            'type' => true
        ]);

        Position::create([
            'name' => 'Compras'
        ]);

        Position::create([
            'name' => 'Comercial'
        ]);

        Position::create([
            'name' => 'Administración'
        ]);
    }
}

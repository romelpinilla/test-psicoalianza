@extends('layouts.default')

@section('content')
    <h1 class="h2">{{ ($action == 'edit')? 'Editar' : 'Nuevo' }} empleado</h1>
    <div class="row my-4">
        <div class="col-12">
            <form novalidate class="needs-validation" action="{{ route('action-employee', [ (isset($employee))? $employee->id : '' ]) }}" method="POST">
                @csrf
                <input type="hidden" name="action" value="{{ $action }}">
                <div class="mb-3">
                    <label class="form-label">Identificación</label>
                    <input class="form-control" type="text" name="dni" value="{{ (isset($employee))? $employee->name : '' }}" required>
                </div>
                <div class="mb-3">
                    <label class="form-label">Nombres</label>
                    <input class="form-control" type="text" name="name" value="{{ (isset($employee))? $employee->name : '' }}" required>
                </div>
                <div class="mb-3">
                    <label class="form-label">Apellidos</label>
                    <input class="form-control" type="text" name="surname" value="{{ (isset($employee))? $employee->surname : '' }}" required>
                </div>
                <div class="mb-3">
                    <label class="form-label">Dirrección</label>
                    <input class="form-control" type="text" name="address" value="{{ (isset($employee))? $employee->address : '' }}" required>
                </div>
                <div class="mb-3">
                    <label class="form-label">Telefono</label>
                    <input class="form-control" type="text" name="phone" value="{{ (isset($employee))? $employee->phone : '' }}" required>
                </div>
                <div class="mb-3">
                    <label class="form-label">Cargos</label>
                    <select id="multiple" class="form-control form-control-chosen w-100" name="positions[]" data-placeholder="Seleccione los cargos del empleado" multiple required>
                        @foreach ($positions as $position)
                            <option value="{{ $position->id }}"
                                {{(isset($employee) && $employee->positions()->where('id', $position->id)->first())? 'selected' : '' }}
                            >{{ $position->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="mb-3">
                    <label class="form-label">Pais</label>
                    <select class="form-control" id="country" name="country" required>
                        <option value="" disabled selected></option>
                        @foreach ($countries as $country)
                            <option value="{{ $country->id }}"
                                {{(isset($employee) && $employee->country_id == $country->id)? 'selected' : '' }}
                            >{{ $country->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="mb-3">
                    <label class="form-label">Ciudad</label>
                    <select class="form-control" id="city" name="city" required>
                        <option value="" disabled selected></option>
                        @foreach ($cities as $city)
                            <option value="{{ $city->id }}"
                                {{(isset($employee) && $employee->city_id == $city->id)? 'selected' : '' }}
                            >{{ $city->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="mb-3">
                    <label class="form-label">Jefe</label>
                    <select class="form-control" name="boss">
                        <option value="" disabled selected></option>
                        @foreach ($bosses as $boss)
                            <option value="{{ $boss->id }}"
                                {{(isset($employee) && $employee->parent_id == $boss->id)? 'selected' : '' }}
                            >{{ $boss->name }} {{ $boss->surname }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="btn-group w-100" role="group">
                    <button type="submit" class="btn btn-fluid btn-sm btn-primary">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check-lg" viewBox="0 0 16 16">
                            <path d="M12.736 3.97a.733.733 0 0 1 1.047 0c.286.289.29.756.01 1.05L7.88 12.01a.733.733 0 0 1-1.065.02L3.217 8.384a.757.757 0 0 1 0-1.06.733.733 0 0 1 1.047 0l3.052 3.093 5.4-6.425a.247.247 0 0 1 .02-.022Z"/>
                        </svg>
                        Guardar
                    </button>
                    <a href="{{ route('employees') }}" class="btn btn-fluid btn-sm btn-outline-danger">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-octagon" viewBox="0 0 16 16">
                            <path d="M4.54.146A.5.5 0 0 1 4.893 0h6.214a.5.5 0 0 1 .353.146l4.394 4.394a.5.5 0 0 1 .146.353v6.214a.5.5 0 0 1-.146.353l-4.394 4.394a.5.5 0 0 1-.353.146H4.893a.5.5 0 0 1-.353-.146L.146 11.46A.5.5 0 0 1 0 11.107V4.893a.5.5 0 0 1 .146-.353L4.54.146zM5.1 1 1 5.1v5.8L5.1 15h5.8l4.1-4.1V5.1L10.9 1H5.1z"/>
                            <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                        </svg>
                        Cancelar
                    </a>
                </div>

            </form>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $('.form-control-chosen').chosen();
        $(document).ready(function(){
            $('#country').change(function(){
                loadCity($(this).find(':selected').val())
            })
        })
        function loadCity(stateId){
            $("#city").children().remove()
            $.ajax({
                type: "GET",
                url: "{{ route('cities') }}/" + stateId
                }).done(function( result ) {
                    console.log(result)
                    $(result).each(function(){
                        console.log(this.id)
                    $("#city").append($('<option>', {
                        value: this.id,
                        text: this.name,
                    }))
                })
            })
        }
    </script>

    <script>
        (function () {
            'use strict'

            var forms = document.querySelectorAll('.needs-validation')

            Array.prototype.slice.call(forms)
            .forEach(function (form) {
                form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                    event.preventDefault()
                    event.stopPropagation()
                }

                form.classList.add('was-validated')
                }, false)
            })
        })()
    </script>
@stop


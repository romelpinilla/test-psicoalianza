<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\PositionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
Route::get('cities/{id?}', [DashboardController::class, 'cities'])->name('cities');

Route::group(['prefix' => 'positions'], function() {
    Route::get('/', [PositionController::class, 'index'])->name('positions');
    Route::get('new', [PositionController::class, 'new'])->name('new-position');
    Route::get('edit/{id}', [PositionController::class, 'edit'])->name('edit-position');
    Route::post('action/{id?}', [PositionController::class, 'action'])->name('action-position');
    Route::get('delete/{id}', [PositionController::class, 'delete'])->name('delete-position');
});

Route::group(['prefix' => 'employees'], function() {
    Route::get('/', [EmployeeController::class, 'index'])->name('employees');
    Route::get('new', [EmployeeController::class, 'new'])->name('new-employee');
    Route::get('edit/{id}', [EmployeeController::class, 'edit'])->name('edit-employee');
    Route::post('action/{id?}', [EmployeeController::class, 'action'])->name('action-employee');
    Route::get('delete/{id}', [EmployeeController::class, 'delete'])->name('delete-employee');
});
